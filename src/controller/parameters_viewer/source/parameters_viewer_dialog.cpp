/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/parameters_viewer_dialog.h"

ParametersViewerDialog::ParametersViewerDialog(QWidget* parent) : QDialog(parent), ui(new Ui::ParametersViewerDialog)
{
  ui->setupUi(this);
}

void ParametersViewerDialog::updateSelectedParameters()
{
  QList<QCheckBox*> controller_checkboxes = ui->groupBox->findChildren<QCheckBox*>();
  QList<QCheckBox*> ekf_checkboxes = ui->groupBox_2->findChildren<QCheckBox*>();
  QList<QCheckBox*> telemetry_checkboxes = ui->groupBox_3->findChildren<QCheckBox*>();

  QStringList updated_telemetry;
  QStringList updated_controller;
  QStringList updated_ekf;

  for (int i = 0; i < controller_checkboxes.size(); ++i)
  {
    if (controller_checkboxes.at(i)->isChecked() && controller_checkboxes.at(i)->text() != "Select All")
      updated_controller << controller_checkboxes.at(i)->text();
  }
  for (int i = 0; i < ekf_checkboxes.size(); ++i)
  {
    if (ekf_checkboxes.at(i)->isChecked() && ekf_checkboxes.at(i)->text() != "Select All")
      updated_ekf << ekf_checkboxes.at(i)->text();
  }
  for (int i = 0; i < telemetry_checkboxes.size(); ++i)
  {
    if (telemetry_checkboxes.at(i)->isChecked() && telemetry_checkboxes.at(i)->text() != "Select All")
      updated_telemetry << telemetry_checkboxes.at(i)->text();
  }

  parameters.clear();

  parameters.insert("Telemetry", updated_telemetry);
  parameters.insert("Ext.Kalman Filter", updated_ekf);
  parameters.insert("Controller", updated_controller);
}

QMap<QString, QStringList> ParametersViewerDialog::getSelectedParameters()
{
  updateSelectedParameters();
  return parameters;
}

ParametersViewerDialog::~ParametersViewerDialog()
{
  delete ui;
}

void ParametersViewerDialog::on_checkBox_47_stateChanged(int arg1)
{
  QList<QCheckBox*> controller_checkboxes = ui->groupBox->findChildren<QCheckBox*>();

  for (int i = 0; i < controller_checkboxes.size(); ++i)
  {
    if (controller_checkboxes.at(i)->text() != "Select All")
      controller_checkboxes.at(i)->setChecked(ui->checkBox_47->isChecked());
  }
}

void ParametersViewerDialog::on_checkBox_46_stateChanged(int arg1)
{
  QList<QCheckBox*> ekf_checkboxes = ui->groupBox_2->findChildren<QCheckBox*>();

  for (int i = 0; i < ekf_checkboxes.size(); ++i)
  {
    if (ekf_checkboxes.at(i)->text() != "Select All")
      ekf_checkboxes.at(i)->setChecked(ui->checkBox_46->isChecked());
  }
}

void ParametersViewerDialog::on_checkBox_45_stateChanged(int arg1)
{
  QList<QCheckBox*> telemetry_checkboxes = ui->groupBox_3->findChildren<QCheckBox*>();

  for (int i = 0; i < telemetry_checkboxes.size(); ++i)
  {
    if (telemetry_checkboxes.at(i)->text() != "Select All")
      telemetry_checkboxes.at(i)->setChecked(ui->checkBox_45->isChecked());
  }
}
