/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/parameter_temporal_series.h"
#include "aerostack_msgs/RequestProcesses.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include <QMessageBox>
#include <cstdio>
#include <geometry_msgs/Vector3Stamped.h>
#include <gtest/gtest.h>

#include <iostream>

ros::NodeHandle* nh;
int total_subtests = 0;
int passed_subtests = 0;
void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}
TEST(GraphicalUserInterfaceTests, parametersViewerTest)
{
  ros::NodeHandle nh;

  std::string estimated_pose_str;
  std::string estimated_speed_str;

  std::string trajectory_controller_pose_str;
  std::string trajectory_controller_speed_str;

  std::string drone_driver_sensor_rotation_angles;

  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher estimated_pose_pub;
  ros::Publisher estimated_speed_pub;

  ros::Publisher trajectory_controller_pose_pub;
  ros::Publisher trajectory_controller_speed_pub;

  ros::Publisher rotation_angles_speed_pub;

  droneMsgsROS::dronePose pose_msg;
  droneMsgsROS::droneSpeeds speed_msg;

  geometry_msgs::Vector3Stamped rotation_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);

  nh.param<std::string>("EstimatedSpeed_droneGMR_wrt_GFF", estimated_speed_str, "EstimatedSpeed_droneGMR_wrt_GFF");
  nh.param<std::string>("EstimatedPose_droneGMR_wrt_GFF", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");
  nh.param<std::string>("trajectoryControllerPositionReferencesRebroadcast", trajectory_controller_pose_str,
                        "trajectoryControllerPositionReferencesRebroadcast");
  nh.param<std::string>("trajectoryControllerSpeedReferencesRebroadcast", trajectory_controller_speed_str,
                        "trajectoryControllerSpeedReferencesRebroadcast");
  nh.param<std::string>("drone_driver_sensor_rotation_angle", drone_driver_sensor_rotation_angles, "rotation_angles");

  system(
      ("xfce4-terminal  \ --tab --title \"Self Localization Selector\"  --command \"bash -c 'roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());
  system(
      ("xfce4-terminal  \ --tab --title \"Pose Adapter\"  --command \"bash -c 'roslaunch pose_adapter_process pose_adapter_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());
  system(
      ("xfce4-terminal  \ --tab --title \"Twist Adapter\"  --command \"bash -c 'roslaunch speeds_adapter_process speeds_adapter_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());
  system(
      ("xfce4-terminal  \ --tab --title \"Process manager\"  --command \"bash -c 'roslaunch process_manager_process process_manager_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());

  system(
      ("xfce4-terminal  \ --tab --title \"Parameters Viewer\"  --command \"bash -c 'roslaunch parameters_viewer parameters_viewer.launch --wait \
             drone_id_namespace:=" +
       drone_id_namespace + " \
             drone_id_int:=" +
       drone_id + "  \
             my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());

  estimated_pose_pub = nh.advertise<droneMsgsROS::dronePose>('/' + drone_id_namespace + '/' + estimated_pose_str, 1);
  estimated_speed_pub =
      nh.advertise<droneMsgsROS::droneSpeeds>('/' + drone_id_namespace + '/' + estimated_speed_str, 1);

  trajectory_controller_pose_pub =
      nh.advertise<droneMsgsROS::dronePose>('/' + drone_id_namespace + '/' + trajectory_controller_pose_str, 1);
  trajectory_controller_speed_pub =
      nh.advertise<droneMsgsROS::droneSpeeds>('/' + drone_id_namespace + '/' + trajectory_controller_speed_str, 1);

  rotation_angles_speed_pub = nh.advertise<geometry_msgs::Vector3Stamped>(
      '/' + drone_id_namespace + '/' + drone_driver_sensor_rotation_angles, 1);

  ros::ServiceClient start_process_cli =
      nh.serviceClient<aerostack_msgs::RequestProcesses>("/" + drone_id_namespace + "/start_processes");
  aerostack_msgs::RequestProcesses::Request req;
  aerostack_msgs::RequestProcesses::Response res;
  req.processes.push_back("self_localization_selector_process");

  pose_msg.x = 1;
  pose_msg.y = 1;
  pose_msg.z = 1;
  pose_msg.roll = -0.349066;
  pose_msg.pitch = 0.785398;
  pose_msg.yaw = 0.523599;

  speed_msg.dx = 2;
  speed_msg.dy = 3;
  speed_msg.dz = 3;
  speed_msg.droll = 1;
  speed_msg.dpitch = 2;
  speed_msg.dyaw = 1;

  while (!res.acknowledge)
  {
    start_process_cli.call(req, res);
  }
  ros::Duration(2).sleep();
  estimated_pose_pub.publish(pose_msg);
  estimated_speed_pub.publish(speed_msg);

  ros::Rate rate(30);
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  std::string response;
  total_subtests++;
  std::cout << "\033[1;34m Are position (1,1,1), YPR (0.52, 0.78 ,-0.34), dpos(2,3,3) and dYPR(1, 2 ,1) shown in Ext. "
               "Kalman Filter? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem receiving pose or speed\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  EXPECT_TRUE(response == "Y" || response == "y");

  trajectory_controller_pose_pub.publish(pose_msg);
  trajectory_controller_speed_pub.publish(speed_msg);
  total_subtests++;
  std::cout << "\033[1;34m Are position ci (1,1,1), YPRci (0.52, 0.78 ,-0.34)  shown in Controller? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem receiving controller information\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  EXPECT_TRUE(response == "Y" || response == "y");

  rotation_msg.vector.z = 20;
  rotation_msg.vector.y = 10;
  rotation_msg.vector.x = 30;
  rotation_angles_speed_pub.publish(rotation_msg);
  total_subtests++;
  std::cout << "\033[1;34m Is rotation YPR (20,10,30)  shown in Telemetry? (Y/N) \033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem receiving telemetry information\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Click on the gear icon in the upper left part of the window. Does a window called "
               "'Parameters' appear ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in diplaying parameters selection window\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Check 'Select all' option in Controller section . Does all options in this section get "
               "checked ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in selecting all members in a section\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Uncheck 'Select all' option in Controller section . Does all options in this section get "
               "unchecked ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in deselecting all members in a section\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Check 'xci' option in the same section and  press 'OK'. Does only appear 'xci' parameter in "
               "'Controller' section? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in changing parameters shown\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Check 'pos.x' option in 'Ext. Kalman Filter'. Does the widget start ploting a red line with "
               "a constant value? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in ploting parameters\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Uncheck 'pos.x' option in 'Ext. Kalman Filter'. Does the red line disappear? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in unploting parameters\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Write '22' in 'Max vertical axis' section. Has the maximum value of Y axis changed to 22? "
               "(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in changing maximum vertical axis value\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Write '-1' in 'Min vertical axis' section. Has the minimum value of Y axis changed to -1? "
               "(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in changing minimum vertical axis value\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Check 'pos.x' and 'pitch' in 'Ext. Kalman Filter' section and press 'Stop'. Has the ploting "
               "been paused? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in stopping plot\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Press 'Save plot' button. Select 'Print to File (PDF) in 'Name'section in 'Print' dialog "
               "that has "
               "just appeard. Press 'Print'. Has a pdf file been created in your work directory with the plot? (Y/N) "
               "\033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in saving plots\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  displaySubtestResults();
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;
  return RUN_ALL_TESTS();
}
