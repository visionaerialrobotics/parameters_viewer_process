/*!*******************************************************************************************
 *  \file       telemetry_state_receiver.h
 *  \brief      UserCommander definition file.
 *  \details    This file includes the TelemetryStateReceiver class declaration. To obtain more
 *              information about it's definition consult the telemetry_state_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef HumanMachineInterface_TELEMETRY_H_
#define HumanMachineInterface_TELEMETRY_H_

#include <ros/ros.h>
#include <string>
#include <vector>
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/Point.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneCommand.h"
#include "geometry_msgs/Twist.h"
#include "aerostack_msgs/AliveSignal.h"

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/FluidPressure.h"
#include "sensor_msgs/Temperature.h"
// Magnetometer and RotationAngles
#include "geometry_msgs/Vector3Stamped.h"

// Battery
#include "droneMsgsROS/battery.h"

// Altitude
#include "droneMsgsROS/droneAltitude.h"

#include "droneMsgsROS/dronePose.h"
// Ground Speed
#include "droneMsgsROS/vector2Stamped.h"

#include <QString>
#include <QThread>
#include <QtDebug>
#include <QStringListModel>

#include <ros/ros.h>
#include <ros/network.h>
#include <std_msgs/String.h>
#include <string>
#include <sstream>

/*****************************************************************************
** Class
*****************************************************************************/

class TelemetryStateReceiver : public QObject
{
  Q_OBJECT
public:
  TelemetryStateReceiver();
  virtual ~TelemetryStateReceiver();

  QString description;
  QString node_name;
  QString error_type;
  QString hostname;
  QString location;
  QString ns;
  QString action;

  QStringList fieldsMsgs;
  droneMsgsROS::dronePitchRollCmd drone_pitchRoll_cmd_msgs;
  droneMsgsROS::droneDAltitudeCmd drone_dAltitude_cmd_msgs;
  droneMsgsROS::droneDYawCmd drone_dyaw_cmd_msgs;
  droneMsgsROS::dronePose drone_pose_msgs;

  geometry_msgs::Vector3Stamped magnetometer_msgs;
  droneMsgsROS::battery battery_msgs;
  droneMsgsROS::droneAltitude altitude_msgs;
  geometry_msgs::Vector3Stamped rotation_angles_msgs;
  droneMsgsROS::vector2Stamped ground_speed_msgs;
  sensor_msgs::Imu imu_msgs;
  droneMsgsROS::droneStatus drone_status_msgs;
  droneMsgsROS::droneCommand drone_command_msgs;
  sensor_msgs::FluidPressure fluid_pressure;
  sensor_msgs::Temperature temperature;

  float time;

  /*!************************************************************************
   *  \brief   This method opens the subscriptions needed by this process.
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

  /*!************************************************************************
   *  \brief   This method checks if the subscriptions have been opened successfully.
   *  \return  Returns true if the subscriptions have been opened successfully, false otherwise.
   ***************************************************************************/
  bool ready();

private:
  bool subscriptions_complete;

  int init_argc;
  int real_time;
  char** init_argv;

  std::string drone_driver_command_drone_command_pitch_roll;
  std::string drone_driver_command_drone_command_daltitude;
  std::string drone_driver_command_drone_command_dyaw;
  std::string drone_driver_command_drone_hl_command;
  std::string drone_driver_sensor_imu;
  std::string drone_driver_sensor_temperature;
  std::string drone_driver_sensor_magnetometer;
  std::string drone_driver_sensor_battery;
  std::string drone_driver_sensor_altitude;
  std::string drone_driver_sensor_rotation_angles;
  std::string drone_driver_sensor_ground_speed;
  std::string drone_driver_sensor_pressure;
  std::string drone_driver_sensor_status;
  std::string supervisor_process_error_unified_notification;
  std::string supervisor_processes_performance;
  std::string drone_pelican_like_simulator_control_input_subscriber;
  std::string drone_okto_like_simulator_okto_commands_subscriber;

  ros::Subscriber drone_command_subs;
  ros::Subscriber drone_pelicanLL_status_subs;
  ros::Subscriber drone_pelican_IMUCalc_subs;
  ros::Subscriber drone_pelicanRCData_subs;
  ros::Subscriber drone_pelicanControl_input_subs;
  ros::Subscriber droneStatusSubs;
  ros::Subscriber magnetometer_subs;
  ros::Subscriber battery_subs;
  ros::Subscriber altitude_subs;
  ros::Subscriber rotation_angles_subs;
  ros::Subscriber ground_speed_subs;
  ros::Subscriber imu_subs;
  ros::Subscriber dronePositionSubs;
  ros::Subscriber drone_HLCmd_subs;
  ros::Subscriber DroneLLCmdSubs;
  ros::Subscriber temperature_subs;
  ros::Subscriber drone_input_subs;
  ros::Subscriber opt_flow_subs;
  ros::Subscriber okto_commands_subs;
  ros::Subscriber pressure_subs;
  ros::Subscriber drone_status_subs;
  ros::Subscriber drone_pitchRoll_cmd_subs;
  ros::Subscriber drone_dAltitude_cmd_subs;
  ros::Subscriber drone_dYaw_cmd_subs;

  /*!************************************************************************
   *  \brief   Receives the drone status
   ***************************************************************************/
  void droneStatusSensorCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the magnetometer messages
   ***************************************************************************/
  void magnetometerCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the battery status.
   ***************************************************************************/
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's altitude.
   ***************************************************************************/
  void altitudeCallback(const droneMsgsROS::droneAltitude::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's rotation angles.
   ***************************************************************************/
  void rotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the ground speeds
   ***************************************************************************/
  void groundSpeedCallback(const droneMsgsROS::vector2Stamped::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the imu sensor messages
   ***************************************************************************/
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's position
   ***************************************************************************/
  void dronePositionCallback(const droneMsgsROS::dronePose::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's low level status
   ***************************************************************************/
  void droneLLCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's high level status
   ***************************************************************************/
  void droneHLCallback(const droneMsgsROS::droneCommand::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the temperature sensor messages
   ***************************************************************************/
  void temperatureCallback(const sensor_msgs::Temperature::ConstPtr& msg);

  /*!************************************************************************
   *  \brief
   ***************************************************************************/
  void droneInputPelicanCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief
   ***************************************************************************/
  void optFlowCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief
   ***************************************************************************/
  void oktoCommandsCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's pressure sensors messages
   ***************************************************************************/
  void pressureCallback(const sensor_msgs::FluidPressure::ConstPtr& msg);

  /*!************************************************************************
   *  \brief
   ***************************************************************************/
  void statusSensorCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's pitch/roll commands
   ***************************************************************************/
  void dronePitchRollCmdCallback(const droneMsgsROS::dronePitchRollCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief
   ***************************************************************************/
  void droneDAltitudeCmdCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the drone's yaw commands
   ***************************************************************************/
  void droneDYawCmdCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg);

Q_SIGNALS:
  void parameterReceived();
  void updateStatus();
};

#endif /* HumanMachineInterface_TELEMETRY_H_ */
