/********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef PARAMETERS_VIEWER_DIALOG_H
#define PARAMETERS_VIEWER_DIALOG_H

#include <QDialog>
#include <QMap>
#include <QStringList>
#include <QList>
#include "ui_parameters_viewer_dialog.h"

namespace Ui
{
class ParametersViewerDialog;
}

class ParametersViewerDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ParametersViewerDialog(QWidget* parent = 0);

  ~ParametersViewerDialog();

  /*!********************************************************************************************************************
   *  \brief      Returns selected checkboxes
   **********************************************************************************************************************/
  QMap<QString, QStringList> getSelectedParameters();

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      Select all parameters on Odometry section
   **********************************************************************************************************************/
  void on_checkBox_47_stateChanged(int arg1);
  /*!********************************************************************************************************************
   *  \brief      Select all parameters on EKF section
   **********************************************************************************************************************/

  void on_checkBox_46_stateChanged(int arg1);
  /*!********************************************************************************************************************
   *  \brief      Select all parameters on Telemetry section
   **********************************************************************************************************************/
  void on_checkBox_45_stateChanged(int arg1);

private:
  Ui::ParametersViewerDialog* ui;

  QMap<QString, QStringList> parameters;

  /*!********************************************************************************************************************
   *  \brief      Updates selected checkboxes
   **********************************************************************************************************************/
  void updateSelectedParameters();
};

#endif  // PARAMETERS_VIEWER_DIALOG_H
