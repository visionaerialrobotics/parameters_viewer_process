/*!*******************************************************************************************
 *  \file       data_plot.h
 *  \brief      DataPlot definition file.
 *  \details    This file includes the DataPlot class declaration. To obtain more
 *              information about it's definition consult the data_plot.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef _DATA_PLOT_H
#define _DATA_PLOT_H 1

#include <qwt_plot.h>
#include <thread>
#include <qwt/qwt_plot_curve.h>
#include <QTreeWidgetItem>
#include <QStringList>
#include <QHash>
#include <QHashIterator>
#include <QList>
#include <QTime>
#include <tf/tf.h>
#include "telemetry_state_receiver.h"
#include "odometry_state_receiver.h"
#include <qwt/qwt_plot_canvas.h>
#include <stdlib.h>
#include <ctime>
#include <QPrintDialog>
#include <QPrinter>
#include <qwt/qwt_plot_renderer.h>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_scale_draw.h>
#include <qwt/qwt_picker.h>
#include <qwt/qwt_math.h>
#include <qwt/qwt_scale_widget.h>
#include <qwt/qwt_scale_draw.h>
#include <qwt/qwt_plot_zoomer.h>
#include <qwt/qwt_plot_panner.h>
#include <qwt/qwt_plot_layout.h>
#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <sstream>

const int PLOT_SIZE = 60;      // 0 to 200
const int BUFFER_SIZE = 120;   // 0 to 200
const int HIGHRATE_SIZE = 20;  // 0 to 200

class DataPlot : public QwtPlot
{
  Q_OBJECT

public:
  DataPlot(QWidget* = NULL, TelemetryStateReceiver* collector = 0, OdometryStateReceiver* odometryReceiver = 0,
           QMap<QString, QStringList>* list = 0);

  /*!********************************************************************************************************************
   *  \brief      This method sets a plain canvas and aligns the scales to it
   **********************************************************************************************************************/
  void alignScales();

  bool connect_status;
  QStringList selected_items;
  QStringList colors_list;
  int iterator_colors;
  bool iconColorChange;  // make difference between change by checkbox and icons.
  bool iconWhiteChange;
  bool is_stop_pressed;
  int data_count;
  int current_max_limit;
  int current_min_limit;

  double roll, pitch, yaw;
  std::vector<std::string> parameters_list;
  QHash<QString, QwtPlotCurve*> curves;
  QHash<QString, QString> colors;
  QHash<QString, int> list_colors_icon;
  QHash<QString, double*> d_y;
  QHash<QString, QTreeWidgetItem*> items;

  /*!********************************************************************************************************************
   *  \brief      This method returns the uptime of this widget
   **********************************************************************************************************************/
  QTime upTime() const;

  /*!********************************************************************************************************************
   *  \brief      This method assigns a color to an item
   **********************************************************************************************************************/
  void assignColorIcon(QString id, QTreeWidgetItem* item);

  /*!********************************************************************************************************************
   *  \brief      This method erases a color of an item
   **********************************************************************************************************************/
  void eraseColorIcon(QString id, QTreeWidgetItem* item);

  /*!********************************************************************************************************************
   *  \brief      This method initiates the Y axis
   **********************************************************************************************************************/
  void initAxisY();

  /*!********************************************************************************************************************
   *  \brief      This method initiates the X axis
   **********************************************************************************************************************/
  void initAxisX();

  /*!********************************************************************************************************************
   *  \brief      This method initiates the time data
   **********************************************************************************************************************/
  void initTimeData();

  /*!********************************************************************************************************************
   *  \brief      This method initiates the grid plot
   **********************************************************************************************************************/
  void setGridPlot();

  /*!********************************************************************************************************************
   *  \brief      This method initiates the curves
   **********************************************************************************************************************/
  void initCurves();

  /*!********************************************************************************************************************
   *  \brief      This method sets the data of a curve
   **********************************************************************************************************************/
  void setDataCurve(double param[], QString curve_id, double data_msg);

  /*!********************************************************************************************************************
   *  \brief      This method sets the labels of the curves
   **********************************************************************************************************************/
  std::vector<std::string> setCurveLabels(QMap<QString, QStringList> list);

  void isConnectionReadyThread();

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This slot sets the timer interval for incoming connections
   **********************************************************************************************************************/
  void setTimerInterval(double interval);

  /*!********************************************************************************************************************
   *  \brief      This method enables or disables the showing of a specific parameter
   **********************************************************************************************************************/
  void clickToPlot(QTreeWidgetItem* item, int colum);

  /*!********************************************************************************************************************
   *  \brief      This method resizes the X axis
   **********************************************************************************************************************/
  void resizeAxisXScale(int ms);

  /*!********************************************************************************************************************
   *  \brief      This method sets the Y axis minimum limit
   **********************************************************************************************************************/
  void resizeAxisYMinLimit(int ms);

  /*!********************************************************************************************************************
   *  \brief      This method sets the Y axis maximum limit
   **********************************************************************************************************************/
  void resizeAxisYMaxLimit(int ms);

  /*!********************************************************************************************************************
   *  \brief      This method method gets the canvas and prints it in the output file
   **********************************************************************************************************************/
  void saveAsSVG();

  /*!********************************************************************************************************************
   *  \brief      This method reads incoming connections
   **********************************************************************************************************************/
  void onParameterReceived();

Q_SIGNALS:

  /*!********************************************************************************************************************
   *  \brief      This signal is emitted while saveAsSVG is being executed
   **********************************************************************************************************************/
  void disconnectUpdateDynamicsView();

  /*!********************************************************************************************************************
   *  \brief      This signal is emitted when saveAsSVG finishes its execution
   **********************************************************************************************************************/
  void connectUpdateDynamicsView();

protected:
  /*!********************************************************************************************************************
   *  \brief      This method fills the parameter values
   **********************************************************************************************************************/
  virtual void timerEvent(QTimerEvent* e);

private:
  std::thread connection_admin_thread;
  double d_x[BUFFER_SIZE];
  double param1[BUFFER_SIZE];
  double param2[BUFFER_SIZE];
  double param3[BUFFER_SIZE];
  double param4[BUFFER_SIZE];
  double param5[BUFFER_SIZE];
  double param6[BUFFER_SIZE];
  double param7[BUFFER_SIZE];
  double param8[BUFFER_SIZE];
  double param9[BUFFER_SIZE];
  double param10[BUFFER_SIZE];
  double param11[BUFFER_SIZE];
  double param12[BUFFER_SIZE];
  double param13[BUFFER_SIZE];
  double param14[BUFFER_SIZE];
  double param15[BUFFER_SIZE];
  double param16[BUFFER_SIZE];
  double param17[BUFFER_SIZE];
  double param18[BUFFER_SIZE];
  double param19[BUFFER_SIZE];
  double param20[BUFFER_SIZE];
  double param21[BUFFER_SIZE];
  double param22[BUFFER_SIZE];
  double param23[BUFFER_SIZE];
  double param24[BUFFER_SIZE];
  double param25[BUFFER_SIZE];
  double param26[BUFFER_SIZE];
  double param27[BUFFER_SIZE];
  double param28[BUFFER_SIZE];
  double param29[BUFFER_SIZE];
  double param30[BUFFER_SIZE];
  double param31[BUFFER_SIZE];
  double param32[BUFFER_SIZE];
  double param33[BUFFER_SIZE];
  double param34[BUFFER_SIZE];
  double param35[BUFFER_SIZE];
  double param36[BUFFER_SIZE];
  double param37[BUFFER_SIZE];
  double param38[BUFFER_SIZE];
  double param39[BUFFER_SIZE];
  double param40[BUFFER_SIZE];
  double param41[BUFFER_SIZE];
  double param42[BUFFER_SIZE];
  double param43[BUFFER_SIZE];
  double param44[BUFFER_SIZE];
  double hrateBuff1[HIGHRATE_SIZE];
  double hrateBuff2[HIGHRATE_SIZE];
  double hrateBuff3[HIGHRATE_SIZE];
  double hrateBuff4[HIGHRATE_SIZE];
  double hrateBuff5[HIGHRATE_SIZE];
  double hrateBuff6[HIGHRATE_SIZE];
  TelemetryStateReceiver* node;
  OdometryStateReceiver* odom_receiver;

  int posicionBuffer;
  int d_interval;  // timer in ms
  int d_timerId;

  QwtPlotCanvas* own_canvas;
};

#endif
